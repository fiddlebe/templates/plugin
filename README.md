# Basic Template

## Table Of Contents

**[Introduction](#Introduction)**  
**[Development](#Development)**  
**[Testing](#Testing)**  
**[Deployment](#Deployment)**


## Introduction

this is an expressui5 template to quick-start your launchpad plugin development.

There's 2 ways to use this template:
*  You can fork this project from the giturl. This has the advantage that you can always rebase your project whenever the template is updated. For Plugins this is actually a recommended approach. The downside is that you will need to change the tempalte placeholders manually to your project namespace.
*  or you can simply download the component project and use it as an expressui5 template. The downside is that your project cannot be rebased when the template changes.

You cannot start a plugin standalone. It must be embedded in a local launchpad for testing. So first have a look at [the fiddle launchpad template](https://gitlab.com/fiddlebe/templates/launchpad).
Once you've setup your local launchpad, you can move to the plugins folder and launch `expressui5` there. Select the plugin template to generate your new plugin.

Now go to the `./appconfig/fioriSandboxConfig.json` file and add a new plugin-section (you can copy paste the issueLogger plugin config and adapt it for your needs).

If you now test your project (local launchpad) your plugin will be loaded.

## Development

I use Visual Studio Code ([link](https://code.visualstudio.com/)) as my local development environment, so please download and install this editor. In theory you are free to choose whatever editor you want.

When you have cloned this repository to your local file-system you can start developing. But don't forget to install the devDependencies first:

```
npm install
```

## Testing

refer to [the fiddle launchpad template](https://gitlab.com/fiddlebe/templates/launchpad).
You may also want to have a look at the [pluginManager-plugin](https://gitlab.com/fiddlebe/ui5/plugins), which allows you to activate/deactivate plugins at runtime, in your launchpad.
Be sure to properly implement the destroyer of your plugin, and remove any created element upon destroy.
Whe your plugin is final, consider publishing it on NPM and registring it in the [Plugin showcase](https://fiddlewookie.github.io/Launchpad/?sap-ushell-sandbox-config=/Launchpad/appconfig/fioriSandboxConfig#Shell-home) by creating a pull request on the [plugins.txt](https://github.com/FiddleWookie/FioriLaunchpadExtensions).

## Deployment

Once you are ready to deploy to the Front-end server you can fill provide the information in the *./project/.sapdeploy.json*-file. This information contains:
* package: Package in which the UI5 application should be placed
* bspontainer: The name of the application (max 15 characters)
* bspcontainer_text: Description of the application
* transportno: The transport request

Don't forget to create a *.sapdeployuser.json*-file to store your credentials, don't get scared, this file **won't be commited/pushed** to git, so this file will only exist locally on your pc.

Once you have provided this data you can deploy to the Front-end server with following command:

```
npm run deploy
```

During deployment, a grunt script will perform basic ESLint checks on your code. All JSDoc comments are parsed into one big json file and moved to the dist(ribution) folder. Preload files are generated and moved to the dist folder. The original files are copied to the dist folder and renamed to -dbg files.
At the end, everything in the dist folder is zipped, and then uploaded to the provided BSP repo in the ABAP system.